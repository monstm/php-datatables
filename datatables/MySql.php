<?php

namespace Samy\DataTables;

use Samy\Sql\MySql as SqlMySql;

/**
 * Simple MySQL implementation.
 */
class MySql extends AbstractDataTables
{
    /**
     * MySql construction.
     *
     * @param[in] string $Host MySQL Host
     * @param[in] string $Username MySQL Password
     * @param[in] string $Password MySQL Username
     * @param[in] string $Database MySQL Database
     * @param[in] int $Port MySQL Port
     *
     * @return void
     */
    public function __construct(string $Host, string $Username, string $Password, string $Database, int $Port = 3306)
    {
        $this->driver = new SqlMySql($Host, $Username, $Password, $Database, $Port);
    }


    /**
     * Retrieve SQL filter expression.
     *
     * @param[in] string $TableName Table name
     * @param[in] string $FieldName Field name
     * @param[in] mixed $Value Data value
     *
     * @return string
     */
    protected function sqlFilter(string $TableName, string $FieldName, mixed $Value): string
    {
        $quote = false;

        if (is_string($Value)) {
            if ($this->driver->isFunction($Value)) {
                $data = $Value;
            } else {
                $data = $this->driver->escape($Value);
                $quote = true;
            }
        } elseif (is_null($Value)) {
            $data = "null";
        } else {
            $data = strval($Value);
        }


        return $this->interpolate("`{table}`.`{field}` = {quote}{data}{quote}", array(
            "table" => $TableName,
            "field" => $FieldName,
            "quote" => ($quote ? "'" : ""),
            "data" => $data
        ));
    }

    /**
     * Retrieve SQL search expression.
     *
     * @param[in] string $TableName Table name
     * @param[in] string $FieldName Field name
     * @param[in] mixed $Value Data value
     * @param[in] bool $Regex Data regex
     *
     * @return string
     */
    protected function sqlSearch(string $TableName, string $FieldName, mixed $Value, bool $Regex): string
    {
        $operator = "=";
        $quote = false;

        if (is_string($Value)) {
            if ($this->driver->isFunction($Value)) {
                $data = $Value;
            } else {
                $quote = true;

                if ($Regex) {
                    $operator = "REGEXP";
                    $data = $this->driver->escape($Value);
                } else {
                    $operator = "LIKE";
                    $data = "%" . $this->driver->escape($Value) . "%";
                }
            }
        } elseif (is_null($Value)) {
            $data = "null";
        } else {
            $data = strval($Value);
        }


        return $this->interpolate("`{table}`.`{field}` {operator} {quote}{data}{quote}", array(
            "table" => $TableName,
            "field" => $FieldName,
            "operator" => $operator,
            "quote" => ($quote ? "'" : ""),
            "data" => $data
        ));
    }

    /**
     * Retrieve SQL order expression.
     *
     * @param[in] string $TableName Table name
     * @param[in] string $FieldName Field name
     * @param[in] bool $Ascending Ascending order
     *
     * @return string
     */
    protected function sqlOrder(string $TableName, string $FieldName, bool $Ascending): string
    {
        return $this->interpolate("`{table}`.`{field}` {order}", array(
            "table" => $TableName,
            "field" => $FieldName,
            "order" => ($Ascending ? "ASC" : "DESC")
        ));
    }


    /**
     * Retrieve record count.
     *
     * @param[in] array $Condition SQL condition
     *
     * @return int
     */
    protected function recordCount(array $Condition): int
    {
        $query = "SELECT COUNT(0) AS `count` " .
            "FROM `" . $this->table . "`" .
            (count($Condition) > 0 ? " WHERE " . implode(" AND ", $Condition) : "");

        $result = $this->driver->query($query);


        return (count($result) > 0 ? $result[0]["count"] : 0);
    }

    /**
     * Retrieve record data.
     *
     * @param[in] array $Condition SQL condition
     * @param[in] array $Order SQL order
     * @param[in] int $Offset SQL offset
     * @param[in] int $Limit SQL limit
     *
     * @return array<array<string, mixed>>
     */
    protected function recordData(array $Condition, array $Order, int $Offset, int $Limit): array
    {
        $table = $this->table;

        $query = "SELECT " . implode(", ", array_map(
            function ($Field) use ($table) {
                return "`" . $table . "`.`" . $Field["name"] . "` AS `" . $Field["alias"] . "`";
            },
            $this->field
        )) .
            " FROM `" . $this->table . "`" .
            (count($Condition) > 0 ? " WHERE " . implode(" AND ", $Condition) : "") .
            (count($Order) > 0 ? " ORDER BY " . implode(", ", $Order) : "") .
            ($Limit > 0 ? " LIMIT " . $Limit : "") .
            ($Offset > 0 ? " OFFSET " . $Offset : "");


        return $this->driver->query($query);
    }
}
