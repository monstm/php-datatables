<?php

namespace Samy\DataTables;

/**
 * This is a simple DataTables implementation that other DataTables can inherit from.
 */
abstract class AbstractDataTables implements DataTablesInterface
{
    /** describe driver */
    protected $driver = null;

    /** describe table name */
    protected $table = "";

    /** describe fields */
    protected $field = array();

    /** describe has schema */
    protected $has_schema = false;

    /** describe filter data */
    protected $filter_data = array();


    /**
     * Retrieve SQL filter expression.
     *
     * @param[in] string $TableName Table name
     * @param[in] string $FieldName Field name
     * @param[in] mixed $Value Data value
     *
     * @return string
     */
    abstract protected function sqlFilter(string $TableName, string $FieldName, mixed $Value): string;

    /**
     * Retrieve SQL search expression.
     *
     * @param[in] string $TableName Table name
     * @param[in] string $FieldName Field name
     * @param[in] mixed $Value Data value
     * @param[in] bool $Regex Data regex
     *
     * @return string
     */
    abstract protected function sqlSearch(string $TableName, string $FieldName, mixed $Value, bool $Regex): string;

    /**
     * Retrieve SQL order expression.
     *
     * @param[in] string $TableName Table name
     * @param[in] string $FieldName Field name
     * @param[in] bool $Ascending Ascending order
     *
     * @return string
     */
    abstract protected function sqlOrder(string $TableName, string $FieldName, bool $Ascending): string;


    /**
     * Retrieve record count.
     *
     * @param[in] array $Condition SQL condition
     *
     * @return int
     */
    abstract protected function recordCount(array $Condition): int;

    /**
     * Retrieve record data.
     *
     * @param[in] array $Condition SQL condition
     * @param[in] array $Order SQL order
     * @param[in] int $Offset SQL offset
     * @param[in] int $Limit SQL limit
     *
     * @return array<array<string, mixed>>
     */
    abstract protected function recordData(array $Condition, array $Order, int $Offset, int $Limit): array;


    /**
     * Interpolates context values into the message placeholders.
     *
     * @param[in] string $message
     * @param[in] array $context
     *
     * @return string
     */
    protected function interpolate(string $message, array $context = array()): string
    {
        $replace_pairs = array();
        foreach ($context as $key => $value) {
            if (is_string($key) && is_string($value)) {
                $replace_pairs["{" . $key . "}"] = $value;
            }
        }

        return strtr($message, $replace_pairs);
    }


    /**
     * Return an instance with provided schema.
     *
     * @param[in] string $Table Table name
     * @param[in] array $Fields Data fields
     *
     * @return static
     */
    public function withSchema(string $Table, array $Fields): self
    {
        $this->table = $Table;

        $this->field = array();
        foreach ($Fields as $key => $value) {
            if (is_string($value)) {
                $name = (is_string($key) ? $key : $value);
                $alias = $value;

                $this->field[$value] = array(
                    "name" => $name,
                    "alias" => $alias
                );
            }
        }

        $this->has_schema = (($this->table != "") && (count($this->field) > 0));

        return $this;
    }

    /**
     * Return an instance with provided filter.
     *
     * @param[in] array $Data Filter data
     *
     * @return static
     */
    public function withFilter(array $Data): self
    {
        $this->filter_data = $Data;

        return $this;
    }


    /**
     * Retrieve DataTables response.
     *
     * @param[in] array $Request DataTables request
     *
     * @return array<string, mixed>
     */
    public function getResponse(array $Request): array
    {
        $ret = array(
            "draw" => intval($Request["draw"] ?? "0"),
            "recordsTotal" => 0,
            "recordsFiltered" => 0,
            "data" => array()
        );

        if ($this->has_schema) {
            $columns = $this->columns($Request["columns"] ?? array());
            $order = $this->order($columns, ($Request["order"] ?? array()));
            $search = $this->search($Request["search"] ?? array());


            $filter_data = $this->filterData();
            $filter_column = $this->filterColumn($columns);
            $filter_search = $this->filterSearch($columns, $search);

            $filter_merge = array_merge($filter_data, $filter_column);
            $implode = implode(" OR ", $filter_search);
            if ($implode != "") {
                array_push($filter_merge, $implode);
            }

            $order_data = $this->orderData($order);


            $ret["recordsTotal"] = $this->recordCount($filter_data);
            $ret["recordsFiltered"] = $this->recordCount($filter_merge);
            $ret["data"] = $this->recordData(
                $filter_merge,
                $order_data,
                intval($Request["start"] ?? "0"),
                intval($Request["length"] ?? "0")
            );
        }

        return $ret;
    }


    /**
     * Escapes special characters in a string for use in an SQL statement.
     *
     * Escapes special characters in the unescaped_string,
     * taking into account the current character set of the connection
     * so that it is safe to place it in a execute() or query().
     * If binary data is to be inserted, this function must be used.
     *
     * This function must always (with few exceptions) be used to make data safe before sending a query.
     *
     * @param[in] string $UnescapedString The string that is to be escaped
     *
     * @return string Escaped query
     */
    public function escape(string $UnescapedString): string
    {
        return ($this->driver->isConnected() ? $this->driver->escape($UnescapedString) : $UnescapedString);
    }


    /**
     * Normalize DataTables columns.
     *
     * @param[in] array $Columns DataTables columns
     *
     * @return array<array<string, mixed>>
     */
    private function columns(array $Columns): array
    {
        $ret = array();

        foreach ($Columns as $column) {
            $key = ($column["data"] ?? "");
            $search = ($column["search"] ?? array());

            if (isset($this->field[$key])) {
                $fieldable = true;
                $field = $this->field[$key];
            } else {
                $fieldable = false;
                $field = "";
            }

            array_push($ret, array(
                "data" => $key,
                "name" => ($column["name"] ?? ""),

                "orderable" => (isset($column["orderable"]) &&
                    is_string($column["searchable"]) ?
                    filter_var(strtolower($column["orderable"]), FILTER_VALIDATE_BOOLEAN) :
                    false
                ),

                "searchable" => (isset($column["searchable"]) &&
                    is_string($column["searchable"]) ?
                    filter_var(strtolower($column["searchable"]), FILTER_VALIDATE_BOOLEAN) :
                    false
                ),
                "search" => array(
                    "value" => (isset($search["value"]) &&
                        is_string($search["value"]) ?
                        $column["search"]["value"] : ""
                    ),
                    "regex" => (isset($search["regex"]) &&
                        is_string($search["regex"]) ?
                        filter_var(strtolower($column["search"]["regex"]), FILTER_VALIDATE_BOOLEAN) :
                        false
                    )
                ),

                "fieldable" => $fieldable,
                "field" => $field
            ));
        }

        return $ret;
    }

    /**
     * Normalize DataTables order.
     *
     * @param[in] array $Columns Normalize columns
     * @param[in] array $Order DataTables order
     *
     * @return array<array<string, mixed>>
     */
    private function order(array $Columns, array $Order): array
    {
        $ret = array();

        foreach ($Order as $data) {
            $pointer = intval($data["column"] ?? "-1");

            if (isset($Columns[$pointer]) && $Columns[$pointer]["orderable"] && $Columns[$pointer]["fieldable"]) {
                array_push($ret, array(
                    "table" => $this->table,
                    "field" => $Columns[$pointer]["field"]["name"],
                    "asc" => (isset($data["dir"]) &&
                        is_string($data["dir"]) &&
                        (strtolower($data["dir"]) == "asc")
                    )
                ));
            }
        }

        return $ret;
    }

    /**
     * Normalize DataTables search.
     *
     * @param[in] array $Search DataTables search
     *
     * @return array<string, mixed>
     */
    private function search(array $Search): array
    {
        return array(
            "value" => ($Search["value"] ?? ""),
            "regex" => (isset($Search["regex"]) && is_string($Search["regex"]) ?
                filter_var(strtolower($Search["regex"]), FILTER_VALIDATE_BOOLEAN) :
                false
            )
        );
    }


    /**
     * Retrieve SQL data filter.
     *
     * @return array<string>
     */
    private function filterData(): array
    {
        $ret = array();

        foreach ($this->filter_data as $field => $data) {
            $result = "";

            switch (gettype($field)) {
                case "integer":
                    if (is_string($data)) {
                        $result = trim($data);
                    }
                    break;
                case "string":
                    $result = trim($this->sqlFilter($this->table, $field, $data));
                    break;
            }

            if ($result != "") {
                array_push($ret, "(" . $result . ")");
            }
        }

        return $ret;
    }

    /**
     * Retrieve SQL column filter.
     *
     * @param[in] array $Columns DataTables columns
     *
     * @return array<string>
     */
    private function filterColumn(array $Columns): array
    {
        $ret = array();

        foreach ($Columns as $column) {
            $is_fieldable = $column["fieldable"] && !empty($column["field"]["name"]);
            $is_searchable = $column["searchable"] && !empty($column["search"]["value"]);
            if ($is_fieldable && $is_searchable) {
                $result = trim($this->sqlSearch(
                    $this->table,
                    $column["field"]["name"],
                    $column["search"]["value"],
                    $column["search"]["regex"]
                ));

                if ($result != "") {
                    array_push($ret, "(" . $result . ")");
                }
            }
        }

        return $ret;
    }

    /**
     * Retrieve SQL search filter.
     *
     * @param[in] array $Columns DataTables columns
     * @param[in] array $Search DataTables search
     *
     * @return array<string>
     */
    private function filterSearch(array $Columns, array $Search): array
    {
        $ret = array();

        $search_value = ($Search["value"] ?? "");
        $search_regex = (isset($Search["regex"]) && is_string($Search["regex"]) ?
            filter_var(strtolower($Search["regex"]), FILTER_VALIDATE_BOOLEAN) :
            false
        );

        if ($search_value != "") {
            foreach ($Columns as $column) {
                $is_fieldable = $column["fieldable"] && !empty($column["field"]["name"]);

                if ($is_fieldable && $column["searchable"]) {
                    $result = trim($this->sqlSearch(
                        $this->table,
                        $column["field"]["name"],
                        $search_value,
                        $search_regex
                    ));

                    if ($result != "") {
                        array_push($ret, "(" . $result . ")");
                    }
                }
            }
        }

        return $ret;
    }


    /**
     * Retrieve SQL data order.
     *
     * @param[in] array $Orders DataTables order
     *
     * @return array<string>
     */
    private function orderData(array $Orders): array
    {
        $ret = array();

        foreach ($Orders as $order) {
            $result = trim($this->sqlOrder($order["table"], $order["field"], $order["asc"]));

            if ($result != "") {
                array_push($ret, $result);
            }
        }

        return $ret;
    }
}
