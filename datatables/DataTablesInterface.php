<?php

namespace Samy\DataTables;

/**
 * Describes DataTables interface.
 */
interface DataTablesInterface
{
    /**
     * Return an instance with provided schema.
     *
     * @param[in] string $TableName Table name
     * @param[in] array $DataFields Data fields
     *
     * @return static
     */
    public function withSchema(string $TableName, array $DataFields): self;

    /**
     * Return an instance with provided filter.
     *
     * @param[in] array $Data Filter data
     *
     * @return static
     */
    public function withFilter(array $Data): self;

    /**
     * Retrieve DataTables response.
     *
     * @param[in] array $Request DataTables request
     *
     * @return array<string, mixed>
     */
    public function getResponse(array $Request): array;


    /**
     * Escapes special characters in a string for use in an SQL statement.
     *
     * Escapes special characters in the unescaped_string,
     * taking into account the current character set of the connection
     * so that it is safe to place it in a execute() or query().
     * If binary data is to be inserted, this function must be used.
     *
     * This function must always (with few exceptions) be used to make data safe before sending a query.
     *
     * @param[in] string $UnescapedString The string that is to be escaped
     *
     * @return string Escaped query
     */
    public function escape(string $UnescapedString): string;
}
