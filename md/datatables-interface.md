# DataTables Interface

Describes DataTables interface.

---

## withSchema

Return an instance with provided schema.

```php
$datatables = $datatables->withSchema($table_name, $data_fields);
```

---

## withFilter

Return an instance with provided filter.

```php
$filter = array(
	"field_string" => "lorem ipsum",
	"field_number" => 123,
	"field_time" => "NOW()",
	"`field_sql` = 'complex query'"
);

$datatables = $datatables->withFilter($filter);
```

---

## getResponse

Retrieve DataTables response.

```php
$response = $datatables->getResponse($_POST);
```

---

## escape

Escapes special characters in a string for use in an SQL statement.

```php
$escaped_string = $datatables->escape($unescaped_string);
```
