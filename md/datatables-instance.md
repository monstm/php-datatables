# DataTables Instance

Simple DataTables Implementation.

---

## MySQL

Implementation MySQL instance.

```php
$datatables = new MySql($host, $username, $password, $database, $port = 3306);
```

---

## PostgreSQL

Implementation PostgreSQL instance.

```php
$datatables = new PostgreSql($host, $username, $password, $database, $port = 5432);
```
