# PHP DataTables

[
	![](https://badgen.net/packagist/v/samy/datatables/latest)
	![](https://badgen.net/packagist/license/samy/datatables)
	![](https://badgen.net/packagist/dt/samy/datatables)
	![](https://badgen.net/packagist/favers/samy/datatables)
](https://packagist.org/packages/samy/datatables)

This is a simple way to implement DataTables server-side processing.

---

## Instalation

### Composer

Composer is a tool for dependency management in PHP.
It allows you to declare the libraries your project depends on and it will manage (install/update) them for you.

```sh
composer require samy/datatables
```

Please see [Composer](https://getcomposer.org/) for more information.

---

## Support

* Repository: <https://gitlab.com/monstm/php-datatables>
* Documentations: <https://monstm.gitlab.io/php-datatables/>
* Annotation: <https://monstm.alwaysdata.net/php-datatables/>
* Issues: <https://gitlab.com/monstm/php-datatables/-/issues>
