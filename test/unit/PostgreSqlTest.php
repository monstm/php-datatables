<?php

namespace Test\Unit;

use Samy\DataTables\PostgreSql as SamyDataTables;
use Samy\Sql\PostgreSql as SamySql;

class PostgreSqlTest extends AbstractDataTables
{
    protected function setUp(): void
    {
        $this->datatables = new SamyDataTables(
            POSTGRESQL_HOST,
            POSTGRESQL_USERNAME,
            POSTGRESQL_PASSWORD,
            POSTGRESQL_DATABASE,
            intval(POSTGRESQL_PORT)
        );
    }


    public function testSqlInit(): void
    {
        $mysql = new SamySql(
            POSTGRESQL_HOST,
            POSTGRESQL_USERNAME,
            POSTGRESQL_PASSWORD,
            POSTGRESQL_DATABASE,
            intval(POSTGRESQL_PORT)
        );

        $is_connected = $mysql->isConnected();

        $this->assertTrue($is_connected);

        if ($is_connected) {
            $query = array(
                "DROP TABLE IF EXISTS " . $this->table_name . " CASCADE",
                "CREATE TABLE " . $this->table_name . "(" .
                    "\"id\" serial NOT NULL, " .
                    "PRIMARY KEY (\"id\"), " .
                    "\"name\" character varying(24) NOT NULL, " .
                    "\"position\" character varying(36) NOT NULL, " .
                    "\"office\" character varying(36) NOT NULL, " .
                    "\"salary\" smallint NOT NULL" .
                    ")"
            );


            $scv = __DIR__ . DIRECTORY_SEPARATOR . "employee.csv";

            if (is_file($scv)) {
                $file = fopen($scv, "r");
                if ($file) {
                    while (($data = fgetcsv($file)) !== false) {
                        $count = count($data);

                        array_push(
                            $query,
                            "INSERT INTO \"" . $this->table_name . "\"" .
                                "(\"id\", \"name\", \"position\", \"office\", \"salary\") " .
                                "VALUES(" .
                                ($count > 0 ? $data[0]  : "0") . ", " .
                                "'" . ($count > 1 ? $mysql->escape($data[1]) : "") . "', " .
                                "'" . ($count > 2 ? $mysql->escape($data[2]) : "") . "', " .
                                "'" . ($count > 3 ? $mysql->escape($data[3]) : "") . "', " .
                                ($count > 4 ? $data[4] : "0") .
                                ") " .
                                "RETURNING \"id\""
                        );
                    }

                    fclose($file);
                }
            }


            foreach ($query as $command) {
                $mysql->execute($command);
            }
        }
    }

    /**
     * @depends testSqlInit
     * @dataProvider \Test\Unit\PostgreSqlDataProvider::dataSqlEscapeString
     */
    public function testEscapeString($UnescapedString, $EscapedString): void
    {
        $this->assertSame($EscapedString, $this->datatables->escape($UnescapedString));
    }
}
