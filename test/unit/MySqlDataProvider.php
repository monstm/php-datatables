<?php

namespace Test\Unit;

class MySqlDataProvider extends DataProvider
{
    // $UnescapedString, $EscapedString
    public function dataSqlEscapeString(): array
    {
        return array(
            array("G;m;SkX]9A7,WkDq", "G;m;SkX]9A7,WkDq"),
            array("xw=rv47llX+s|*S*", "xw=rv47llX+s|*S*"),
            array("EXT6%xzp1ksb8MJ_", "EXT6%xzp1ksb8MJ_"),
            array("U6`2bOMpi6wN?t#j", "U6`2bOMpi6wN?t#j"),
            array("f7vT/BXYoA]1xwH(", "f7vT/BXYoA]1xwH("),
            array("Hlq4`sR?eR-6HjGh", "Hlq4`sR?eR-6HjGh"),
            array("jrQ;fgw4)%<I4rx~", "jrQ;fgw4)%<I4rx~"),
            array("k,`XKo*4X%#Bl6an", "k,`XKo*4X%#Bl6an"),
            array("zoL54&<fa'I!]I6p", "zoL54&<fa\\'I!]I6p"),
            array("b)D9#xue&|>\"r<~2", "b)D9#xue&|>\\\"r<~2"),

            array("W08elQ\"~7*ed[zw:", "W08elQ\\\"~7*ed[zw:"),
            array("b;_A(i\$wvY_Bx4)g", "b;_A(i\$wvY_Bx4)g"),
            array("kWUROa@Nu0>&vWtD", "kWUROa@Nu0>&vWtD"),
            array("UykZ'G(B1Si!v:cs", "UykZ\\'G(B1Si!v:cs"),
            array("YB{3yQv*;,q08DdL", "YB{3yQv*;,q08DdL"),
            array("XG@gx;V*@ik5i7\"D", "XG@gx;V*@ik5i7\\\"D"),
            array("B!QP*DQ#FN[1yt7l", "B!QP*DQ#FN[1yt7l"),
            array("wWevt]a}8&g)JF>h", "wWevt]a}8&g)JF>h"),
            array("z>#=j*25pk+K~V[z", "z>#=j*25pk+K~V[z"),
            array("T<efXP\")4?!J,#:E", "T<efXP\\\")4?!J,#:E"),

            array("zfa0&AOU~Q7(K3M,", "zfa0&AOU~Q7(K3M,"),
            array("RLa_H8Al]'b]l94;", "RLa_H8Al]\\'b]l94;"),
            array("FwEc9VGb=bUo[R~Y", "FwEc9VGb=bUo[R~Y"),
            array("N060Tmu4&By\$:C0!", "N060Tmu4&By\$:C0!"),
            array("GrANL+6Jw`onLY3}", "GrANL+6Jw`onLY3}"),
            array("O+r;)n1ket3;OQ*7", "O+r;)n1ket3;OQ*7"),
            array("kh>p6>SkOR1CH4(:", "kh>p6>SkOR1CH4(:"),
            array("Xm[4Sa,u~]Svs%N5", "Xm[4Sa,u~]Svs%N5"),
            array("b%N<sbM705_n,jX*", "b%N<sbM705_n,jX*"),
            array("gx'I8)y6l6bXx.M;", "gx\\'I8)y6l6bXx.M;"),

            array("B5Lvoke1R('~G(93", "B5Lvoke1R(\\'~G(93"),
            array("CvGUeNQU#w%^z-5'", "CvGUeNQU#w%^z-5\\'"),
            array("kI8'2Cw9'bgKn&7~", "kI8\\'2Cw9\\'bgKn&7~"),
            array("HPau~7_gs%PTf7on", "HPau~7_gs%PTf7on"),
            array("uTJ2?T/J]zm,GSNv", "uTJ2?T/J]zm,GSNv"),
            array("a];Xnt^ve`1Z;XUx", "a];Xnt^ve`1Z;XUx"),
            array("fb8?)v~T7\"2MW'`q", "fb8?)v~T7\\\"2MW\\'`q"),
            array("I4rI4t(~6J}H.>oD", "I4rI4t(~6J}H.>oD"),
            array("Adr>Z%sP~8\"7Bv'8", "Adr>Z%sP~8\\\"7Bv\\'8"),
            array("CG\$w#8F]1TWlq^C#", "CG\$w#8F]1TWlq^C#"),

            array("cT@koJh19bfL2d+u", "cT@koJh19bfL2d+u"),
            array("fBu&=X?5s<@E2J@n", "fBu&=X?5s<@E2J@n"),
            array("K92_H4ZQp.iZ2^mx", "K92_H4ZQp.iZ2^mx"),
            array("a7'pwLS)eO-4P\$';", "a7\\'pwLS)eO-4P\$\\';"),
            array("BD@M25)z%q+rjt?\"", "BD@M25)z%q+rjt?\\\""),
            array("rdGc'YG#+.n\"Nj3b", "rdGc\\'YG#+.n\\\"Nj3b"),
            array("D[O6'kUa#3;(/T,r", "D[O6\\'kUa#3;(/T,r"),
            array("LZCCb7qka<wp]w>[", "LZCCb7qka<wp]w>["),
            array("Zs2UTO~&tibS7l#N", "Zs2UTO~&tibS7l#N"),
            array("I~:5lLK3,<@i:FIs", "I~:5lLK3,<@i:FIs")
        );
    }
}
