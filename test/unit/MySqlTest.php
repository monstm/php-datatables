<?php

namespace Test\Unit;

use Samy\DataTables\MySql as SamyDataTables;
use Samy\Sql\MySql as SamySql;

class MySqlTest extends AbstractDataTables
{
    protected function setUp(): void
    {
        $this->datatables = new SamyDataTables(
            MYSQL_HOST,
            MYSQL_USERNAME,
            MYSQL_PASSWORD,
            MYSQL_DATABASE,
            intval(MYSQL_PORT)
        );
    }


    public function testSqlInit(): void
    {
        $mysql = new SamySql(
            MYSQL_HOST,
            MYSQL_USERNAME,
            MYSQL_PASSWORD,
            MYSQL_DATABASE,
            intval(MYSQL_PORT)
        );

        $is_connected = $mysql->isConnected();

        $this->assertTrue($is_connected);

        if ($is_connected) {
            $query = array(
                "DROP TABLE IF EXISTS `" . $this->table_name . "`",
                "CREATE TABLE `" . $this->table_name . "`(" .
                    "`id` int unsigned NOT NULL AUTO_INCREMENT PRIMARY KEY, " .
                    "`name` varchar(24) NOT NULL, " .
                    "`position` varchar(36) NOT NULL, " .
                    "`office` varchar(36) NOT NULL, " .
                    "`salary` tinyint unsigned NOT NULL" .
                    ")"
            );


            $scv = __DIR__ . DIRECTORY_SEPARATOR . "employee.csv";

            if (is_file($scv)) {
                $file = fopen($scv, "r");
                if ($file) {
                    while (($data = fgetcsv($file)) !== false) {
                        $count = count($data);

                        array_push(
                            $query,
                            "INSERT INTO `" . $this->table_name . "`(`id`, `name`, `position`, `office`, `salary`) " .
                                "VALUES(" .
                                ($count > 0 ? $data[0]  : "0") . ", " .
                                "'" . ($count > 1 ? $mysql->escape($data[1]) : "") . "', " .
                                "'" . ($count > 2 ? $mysql->escape($data[2]) : "") . "', " .
                                "'" . ($count > 3 ? $mysql->escape($data[3]) : "") . "', " .
                                ($count > 4 ? $data[4] : "0") .
                                ")"
                        );
                    }

                    fclose($file);
                }
            }


            foreach ($query as $command) {
                $mysql->execute($command);
            }
        }
    }

    /**
     * @depends testSqlInit
     * @dataProvider \Test\Unit\MySqlDataProvider::dataSqlEscapeString
     */
    public function testEscapeString($UnescapedString, $EscapedString): void
    {
        $this->assertSame($EscapedString, $this->datatables->escape($UnescapedString));
    }
}
