<?php

namespace Test\Unit;

class DataProvider
{
    // $Filter, $Request, $Draw, $RecordsTotal, $RecordsFiltered, $Data
    public function dataResponse(): array
    {
        $ret = array();

        $filename = __DIR__ . DIRECTORY_SEPARATOR . "response.json";

        if (is_file($filename)) {
            $json = json_decode(file_get_contents($filename), true);
            if (is_array($json)) {
                foreach ($json as $index => $data) {
                    $draw = $index + 1;

                    $request = array(
                        "draw" => $draw,
                        "columns" => array(),
                        "order" => ($data["order"] ?? array(array("column" => "0", "dir" => "asc"))),
                        "start" => strval($data["start"] ?? 0),
                        "length" => strval($data["length"] ?? 10),
                        "search" => ($data["order"] ?? array(array("value" => "", "regex" => "false")))
                    );

                    foreach (($data["column"] ?? array()) as $column_data => $column_search_value) {
                        array_push($request["columns"], array(
                            "data" => $column_data,
                            "name" => "",
                            "searchable" => "true",
                            "orderable" => "true",
                            "search" => array(
                                "value" => $column_search_value,
                                "regex" => "false"
                            )
                        ));
                    }

                    array_push($ret, array(
                        ($data["filter"] ?? array()), // $Filter
                        $request, // $Request
                        $draw, // $Draw
                        ($data["records_total"] ?? 0), // $RecordsTotal
                        ($data["records_filtered"] ?? 0), // $RecordsFiltered
                        ($data["data"] ?? array()) // $Data
                    ));
                }
            }
        }

        return $ret;
    }
}
