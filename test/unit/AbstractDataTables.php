<?php

namespace Test\Unit;

use PHPUnit\Framework\TestCase;

class AbstractDataTables extends TestCase
{
    protected $datatables;

    protected $table_name = "php_datatables_employee";
    protected $data_fieds = array("name", "position", "office", "salary");


    /**
     * @depends testSqlInit
     * @dataProvider \Test\Unit\DataProvider::dataResponse
     */
    public function testResponse($Filter, $Request, $Draw, $RecordsTotal, $RecordsFiltered, $Data): void
    {
        $response = $this->datatables
            ->withSchema($this->table_name, $this->data_fieds)
            ->withFilter($Filter)
            ->getResponse($Request);

        $this->assertResponse($Draw, "draw", $response);
        $this->assertResponse($RecordsTotal, "recordsTotal", $response);
        $this->assertResponse($RecordsFiltered, "recordsFiltered", $response);
        $this->assertResponse($Data, "data", $response);
    }

    private function assertResponse($Expect, $Key, $Response): void
    {
        $this->assertArrayHasKey($Key, $Response);

        if (isset($Response[$Key])) {
            $this->assertSame($Expect, $Response[$Key]);
        }
    }
}
